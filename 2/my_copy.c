#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
int main(int argc, char **argv){
	FILE *file1;
	FILE *file2;
	
	file1 = open(argv[1], O_RDONLY);
	file2 = open(argv[2], O_WRONLY| O_CREAT| O_TRUNC);
	
	if (file1 == NULL){
		fprintf(stderr, "File %s could not be opened\n", file1);
		exit(1);
	}
	if (file2 == NULL){
		fprintf(stderr, "File %s could not be opened\n", file2);
		exit(1);
	}
	
	char buffer[10240];
	unsigned int lines1;
	
	while((lines1 = read(file1, buffer, sizeof buffer)) > 0){
		unsigned int lines2 = write(file2, buffer, lines1);		
	}
	close(file1);
	close(file2);
}

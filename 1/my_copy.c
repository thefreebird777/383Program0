#include <stdlib.h>
#include <stdio.h>
int main(int argc, char **argv){
	FILE *file1;
	FILE *file2;
	
	file1 = fopen(argv[1], "r");
	file2 = fopen(argv[2], "w");
	
	if (file1 == NULL){
		fprintf(stderr, "File %s could not be opened\n", file1);
		exit(1);
	}

	if (file2 == NULL){
		fprintf(stderr, "File %s could not be opened\n", file2);
		exit(1);
	}
	
	char buffer[10240];
	unsigned int lines1;
	
	while((lines1 = fread(buffer,1, sizeof buffer, file1)) > 0){
		unsigned int lines2 = fwrite(buffer, 1, lines1, file2);		
	}
	fclose(file1);
	fclose(file2);
}

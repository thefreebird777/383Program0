#ifndef DSLIB_H_INCLUDED
#define DSLIB_H_INCLUDED

typedef struct stack {
        int *arr;
        int size;
        int MAX;
} stack;

void stack_init(stack *s, int capacity);

int stack_size(stack *s);

int stack_pop(stack *s);

void stack_push(stack *s, int e);

void stack_deallocate(stack *s);

#endif

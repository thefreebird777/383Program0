#include "dslib.h"
#include <stdlib.h>
int main(){
	int CAP = 10;
	struct stack *s;
	stack_init(s,CAP);
	stack_push(s, 556);
	stack_push(s, 73);
	stack_push(s, 85);
	stack_push(s, 4);
	stack_push(s, 100);
	stack_push(s, 21);
	printf("%d\n", stack_size(s));
	printf("%d\n", stack_pop(s));
	printf("%d\n", stack_size(s));
	printf("%d\n", stack_pop(s));
        printf("%d\n", stack_size(s));
	printf("%d\n", stack_pop(s));
        printf("%d\n", stack_size(s));
	printf("%d\n", stack_pop(s));
        printf("%d\n", stack_size(s));
	printf("%d\n", stack_pop(s));
        printf("%d\n", stack_size(s));
	printf("%d\n", stack_pop(s));
        printf("%d\n", stack_size(s));

	stack_deallocate(s);

	return 0;
}

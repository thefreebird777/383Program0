#include "dslib.h"
#include <stdlib.h>
void stack_init(stack *s, int capacity){
	int newStack[capacity];
	*newStack = malloc(sizeof(int)*capacity);
	s->arr = *newStack;
	s->size=0;
	s->MAX=capacity;
}

int stack_size(stack *s){
	return s->size;
}

int stack_pop(stack *s){
	if (s->size == 0){
		printf("Stack is empty.\n");
		return -9999;
	}
	else {
		s->size--;
		int temp = s->arr[s->size+1];
		s->arr[s->size+1] = -9999;
		return temp;
	}
}

void stack_push(stack *s, int e){
	if (s->size < s->MAX){ 
		s->size++;
		s->arr[s->size] = e;
	}
	else {
		printf("Stack is full\n");
		return;
	}
}

void stack_deallocate(stack *s){
	free(s->arr);
	s->size=0;
	s->MAX=0;
}

